#ifndef LIB6502_6502_H
#define LIB6502_6502_H

#include <cstdint>

struct CPU_6502
{
    uint8_t A;
    uint8_t X;
    uint8_t Y;

    bool C;
    bool V;
    bool N;
    bool Z;
    bool D;

    void clc()
    {
        C = false;
    }

    void sec()
    {
        C = true;
    }
};

#endif //LIB6502_6502_H
