#include <6502.h>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("construct", "[construct]")
{
    CPU_6502 cpu{};
}

TEST_CASE("flags", "[instructions]")
{
    CPU_6502 cpu{};
    SECTION("carry")
    {
        cpu.C = false;
        cpu.sec();
        REQUIRE(cpu.C == true);
        cpu.clc();
        REQUIRE(cpu.C == false);
    }
}
